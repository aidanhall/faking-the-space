#include "tecs-system.hpp"
#include "tecs.hpp"
#include <array>
#include <cstdint>
#include <functional>
#include <glm/glm.hpp>
#include <iostream>
#include <optional>
#include <random>
#include <raylib.h>

Sound sound_join, sound_quit, sound_done;

constexpr int WINDOW_WIDTH = 1280;
constexpr int WINDOW_HEIGHT = 720;

constexpr int WORLD_WIDTH = 16;
constexpr int WORLD_HEIGHT = WORLD_WIDTH;
constexpr int TILE_GAP = 1;
constexpr int TILE_SIZE = 32;
constexpr double GAME_DURATION = 300;
constexpr float CHARACTER_SPAWN_RATE = 0.2f;
double game_start;

int points = 0;
constexpr int MAX_AVAILABLE_AREAS = 20;
int available_areas = MAX_AVAILABLE_AREAS;

enum AreaLoaded {
  UNLOADED = 0,
  LOADED = 1,
};

const std::array area_colours = {
    DARKGRAY,
    LIGHTGRAY,
};

std::array<std::array<enum AreaLoaded, WORLD_WIDTH>, WORLD_HEIGHT> world{};
glm::vec2 world_pos = {20, 20};

std::random_device dev;
std::mt19937 generator(dev());

// Have a minimum value for the colours so the character is always visible.
std::uniform_int_distribution<uint8_t> colour_value_distribution(64, 255);
auto random_colour_value = std::bind(colour_value_distribution, generator);

std::uniform_int_distribution<uint8_t>
    x_coordinate_distribution(0, WORLD_WIDTH - 1);
auto random_area_x = std::bind(x_coordinate_distribution, generator);
// std::uniform_int_distribution<uint8_t>
//     y_coordinate_distribution(0, WORLD_HEIGHT - 1);
// auto random_area_y = std::bind(y_coordinate_distribution, generator);

std::uniform_real_distribution<float> pm_one_distribution(-1, 1);
auto random_pm_one = std::bind(pm_one_distribution, generator);

std::uniform_real_distribution<float> tile_offset(0, TILE_SIZE);
auto random_tile_offset = std::bind(tile_offset, generator);

std::uniform_real_distribution<float> zero_one_distribution(0, 1);
auto random_zero_one = std::bind(zero_one_distribution, generator);

std::normal_distribution<float> standard_normal_distribution(0, 1);
auto random_normal_distribution =
    std::bind(standard_normal_distribution, generator);

glm::vec2 random_area_coords() {
  return glm::vec2{random_area_x(), random_area_x()} *
             static_cast<float>(TILE_SIZE + TILE_GAP) +
         world_pos + glm::vec2{random_tile_offset(), random_tile_offset()};
}

// Assumes coords are valid (within the bounds of the world)
glm::vec2 get_pixel_at_area(glm::vec2 coords) {
  return coords * static_cast<float>(TILE_SIZE + TILE_GAP);
}

// Coordinates of the area in the world the pixel is on, or nullopt if it's not
// on the world.
std::optional<glm::vec2> get_area_at_pixel(glm::vec2 pixel) {
  const auto coords =
      (pixel - world_pos) / static_cast<float>(TILE_SIZE + TILE_GAP);
  if (coords.x < 0 || coords.x >= WORLD_WIDTH || coords.y < 0 ||
      coords.y >= WORLD_HEIGHT) {
    return std::nullopt;
  } else {
    return glm::vec2{std::floor(coords.x), std::floor(coords.y)};
  }
}

auto draw_world(glm::vec2 top_left, float tile_size) {
  for (auto y = 0; y < world.size(); y++) {
    const auto &row = world[y];
    for (auto x = 0; x < row.size(); x++) {
      const auto &tile = row[x];
      const auto pos = top_left + (glm::vec2{x, y}) * (tile_size + TILE_GAP);
      DrawRectangleRounded({pos.x, pos.y, tile_size, tile_size}, 0.1f, 0,
                           area_colours[tile]);
    }
  }
}

struct Velocity {
  glm::vec2 value;
};

struct Transform2D {
  glm::vec2 position;
  float rotation;
  glm::vec2 scale;
};

struct Sprite {
  Texture texture;
  glm::vec2 size;
  Color tint = WHITE;
};

enum CharacterBehaviour {
  TARGETTING,
  WANDERING,
};

struct Character {
  // How fast character moves towards its target;
  float target_speed;
  // Last time character veered off course.
  float last_divergence;
  // Average time character goes without veering off course.
  float focus;
  // How much character veers off-course while moving towards their target
  // (radians).
  float jitter = std::numbers::pi / 4.0;
  float turn_speed;
  glm::vec2 target;
  // enum CharacterBehaviour behaviour = TARGETTING;
  float frustration;
  float max_frustration;
};

auto draw_sprite(Tecs::Coordinator &ecs, const Tecs::Entity entity) {
  const auto &sprite = ecs.getComponent<Sprite>(entity);
  const auto &[position, rotation, scale] =
      ecs.getComponent<Transform2D>(entity);
  auto scaled = Rectangle{position.x, position.y, sprite.size.x * scale.x,
                          sprite.size.y * scale.y};
  DrawTexturePro(sprite.texture, Rectangle{0, 0, sprite.size.x, sprite.size.y},
                 scaled, {scaled.width / 2.0f, scaled.height / 2.0f},
                 360.0f * (rotation - std::numbers::pi / 2) /
                     (2 * std::numbers::pi),
                 sprite.tint);
}

auto draw_character_target(Tecs::Coordinator &ecs, const Tecs::Entity entity) {
  const auto &[position, rotation, scale] =
      ecs.getComponent<Transform2D>(entity);
  const auto &character = ecs.getComponent<Character>(entity);
  const auto &sprite = ecs.getComponent<Sprite>(entity);
  DrawCircleLines(character.target.x, character.target.y, 6.0f, sprite.tint);
  DrawLine(position.x, position.y, character.target.x, character.target.y,
           sprite.tint);
}

auto draw_frustration(Tecs::Coordinator &ecs, const Tecs::Entity entity) {
  const auto &transform = ecs.getComponent<Transform2D>(entity);
  const auto &character = ecs.getComponent<Character>(entity);
  if (character.frustration > 0) {
    DrawRectangle(transform.position.x - 9, transform.position.y - 9, 17, 6,
                  YELLOW);
    DrawRectangle(transform.position.x - 8, transform.position.y - 8,
                  16 * character.frustration / character.max_frustration, 4,
                  RED);
  }
}

auto character_ai(Tecs::Coordinator &ecs, const Tecs::Entity entity) {
  auto &character = ecs.getComponent<Character>(entity);
  auto &transform = ecs.getComponent<Transform2D>(entity);
  // Normalise rotation
  while (transform.rotation < 0) {
    transform.rotation += std::numbers::pi * 2;
  }
  while (transform.rotation > std::numbers::pi * 2) {
    transform.rotation -= std::numbers::pi * 2;
  }
  const auto delta = GetFrameTime();
  const auto area = get_area_at_pixel(transform.position);
  if (!area.has_value()) {
    PlaySound(sound_quit);
    ecs.queueDestroyEntity(entity);
    return;
  }

  if (glm::length(transform.position - character.target) < 8.0) {
    PlaySound(sound_done);
    points += 1;
    ecs.queueDestroyEntity(entity);
  }

  if (world[area->y][area->x] == LOADED) {
    character.frustration = 0;
    const float time = GetTime();

    float target_direction;

    if (time < (random_normal_distribution() * character.jitter) +
                   character.focus + character.last_divergence) {
      target_direction = std::atan2(character.target.y - transform.position.y,
                                    character.target.x - transform.position.x);
    } else {
      character.last_divergence = time;
      target_direction = std::numbers::pi * 2 * random_zero_one();
    }

    transform.rotation = std::lerp(transform.rotation, target_direction,
                                   character.turn_speed * delta);

    auto velocity =
        glm::vec2{std::cos(transform.rotation), std::sin(transform.rotation)} *
        character.target_speed;
    transform.position += velocity * delta;
  } else {
    character.frustration += delta;
    if (character.frustration >= character.max_frustration) {
      PlaySound(sound_quit);
      ecs.queueDestroyEntity(entity);
    }
  }
}

Texture player_texture;

auto spawn_player(Tecs::Coordinator &ecs) {

  PlaySound(sound_join);
  const auto player = ecs.newEntity();
  Color col = {
      random_colour_value(),
      random_colour_value(),
      random_colour_value(),
      255,
  };
  std::cout << col.r << ", " << col.g << ", " << col.b << ", " << col.a << '\n';
  ecs.addComponents(player,
                    Sprite{
                        player_texture,
                        {16, 16},
                        col,
                    },
                    Transform2D{random_area_coords(), 0.0f, {1, 1}},
                    Character{12 + random_normal_distribution() * 2, static_cast<float>(GetTime()),
                              15, std::numbers::pi / 8.0,
                              std::numbers::pi / 1.5, random_area_coords(), 0,
                              5 + 2 * random_normal_distribution()});
}

int main() {
  InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Faking the Space");
  InitAudioDevice();
  SetTargetFPS(60);

  player_texture = LoadTexture("player.png");
  sound_join = LoadSound("join.ogg");
  sound_quit = LoadSound("quit.ogg");
  sound_done = LoadSound("done.ogg");

  while (!WindowShouldClose()) {
    // Clear world for multiple sessions
    world = {};
    available_areas = MAX_AVAILABLE_AREAS;
    Tecs::Coordinator ecs;

    Tecs::registerSystemComponents(ecs);

    const auto TRANSFORM2D_COMPONENT = ecs.registerComponent<Transform2D>();
    const auto SPRITE_COMPONENT = ecs.registerComponent<Sprite>();
    const auto CHARACTER_COMPONENT = ecs.registerComponent<Character>();
    const auto VELOCITY_COMPONENT = ecs.registerComponent<Velocity>();

    const auto RENDER_SYSTEM_COMPONENT_ID = ecs.registerComponentId();
    const Tecs::ComponentMask RENDER_SYSTEM_COMPONENT =
        1 << RENDER_SYSTEM_COMPONENT_ID;

    const auto AI_SYSTEM_COMPONENT_ID = ecs.registerComponentId();
    const auto UI_SYSTEM_COMPONENT_ID = ecs.registerComponentId();

    const auto logic_system_interest =
        Tecs::makeSystemInterest(ecs, 1 << AI_SYSTEM_COMPONENT_ID);
    ecs.addComponents(ecs.newEntity(), AI_SYSTEM_COMPONENT_ID,
                      Tecs::InterestedClient{ecs.interests.registerInterests(
                          {{TRANSFORM2D_COMPONENT | CHARACTER_COMPONENT}})},
                      Tecs::PerEntitySystem{character_ai});

    const auto render_system_interest =
        Tecs::makeSystemInterest(ecs, RENDER_SYSTEM_COMPONENT);
    const auto ui_system_interest =
        Tecs::makeSystemInterest(ecs, 1 << UI_SYSTEM_COMPONENT_ID);

    ecs.addComponents(ecs.newEntity(), UI_SYSTEM_COMPONENT_ID,
                      Tecs::InterestedClient{ecs.interests.registerInterests(
                          {{TRANSFORM2D_COMPONENT | CHARACTER_COMPONENT}})},
                      Tecs::PerEntitySystem{draw_frustration});

    ecs.addComponents(ecs.newEntity(), RENDER_SYSTEM_COMPONENT_ID,
                      Tecs::InterestedClient{ecs.interests.registerInterests(
                          {{TRANSFORM2D_COMPONENT | CHARACTER_COMPONENT}})},
                      Tecs::PerEntitySystem{draw_character_target});

    ecs.addComponents(ecs.newEntity(), RENDER_SYSTEM_COMPONENT_ID,
                      Tecs::InterestedClient{ecs.interests.registerInterests(
                          {{TRANSFORM2D_COMPONENT | SPRITE_COMPONENT}})},
                      Tecs::PerEntitySystem{draw_sprite});

    spawn_player(ecs);
    game_start = GetTime();
    while (GetTime() < game_start + GAME_DURATION) {
      if (WindowShouldClose()) {
        goto cleanup;
      }

      Tecs::runSystems(ecs, logic_system_interest);
      if (random_zero_one() < CHARACTER_SPAWN_RATE * GetFrameTime())
        spawn_player(ecs);

      BeginDrawing();
      ClearBackground(BLACK);
      draw_world(world_pos, TILE_SIZE);
      const auto area = get_area_at_pixel({GetMouseX(), GetMouseY()});
      Tecs::runSystems(ecs, render_system_interest);
      Tecs::runSystems(ecs, ui_system_interest);

      if (area.has_value()) {
        DrawText((std::string("Coords: ") + std::to_string(area->x) + ", " +
                  std::to_string(area->y))
                     .c_str(),
                 900, 600, 10, WHITE);
        const auto area_state = world[area->y][area->x];
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && area_state == UNLOADED &&
            available_areas > 0) {
          world[area->y][area->x] = LOADED;
          available_areas -= 1;
        }
        if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON) && area_state == LOADED) {
          world[area->y][area->x] = UNLOADED;
          available_areas += 1;
        }
      }

      DrawText((std::string("Points: ") + std::to_string(points)).c_str(), 900,
               500, 30, WHITE);
      DrawText((std::string("Time: ") +
                std::to_string(static_cast<int>(game_start + GAME_DURATION - GetTime())) +
                "s")
                   .c_str(),
               900, 400, 30, WHITE);
      DrawText((std::string("Free Memory: ") + std::to_string(available_areas))
                   .c_str(),
               600, 300, 50, WHITE);
      EndDrawing();

      ecs.destroyQueued();
    }

    while (!IsKeyPressed(KEY_SPACE)) {
      if (WindowShouldClose())
        goto cleanup;

      BeginDrawing();
      ClearBackground(BLACK);
      DrawText((std::string("You got ") + std::to_string(points) +
                std::string(                " points!")).c_str(),
               200, 200, 80, WHITE);
      DrawText("Press Space to Restart",
               200, 300, 80, WHITE);
      EndDrawing();
    }
  }

cleanup:
  UnloadTexture(player_texture);
  CloseWindow();
}
