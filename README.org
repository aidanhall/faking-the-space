#+title: Roles Reversed Game Jam
* TODO Make a Cool Game
DEADLINE: <2023-07-09 Sun 18:00>
- Submit it on [[https://itch.io/jam/gmtk-2023][Itch.io]].
* Ideas and Inspiration
** Embody A Game Mechanic
Reverse the roles of player and game.
- [[https://itch.io/jam/gmtk-2023/rate/2079326][Brain Train]] already did it, 2 hours after the theme announcement?!
  (Looks like an asset flip.)
*** Fast Travel Switch-Board
- Fast travel occurs across wires and you hook them up.
- /Too/ abstract: Divorced from the other role.
- Already a thing.
*** Potion Seller/NPC/Shop
Already a Thing
*** Make an RPG Dungeon
Already a Thing: Boss monster/tower defence.
*** Scene Loading
- You're a little man inside the computer, loading and unloading areas in a game as virtual characters move around the world.  When a character enters an area that's not loaded, they wait, and their patience depletes (patience is restored after the area is loaded).   If a character's patience runs out, they quit the game, and you are graded based on how many characters reach their objectives successfully.
- Characters can only move if they are in a loaded area, so timing can be used to grade players, as well as/instead of characters leaving.
- Alternative options:
  - Instant Loading ::  A character will leave immediately if they move into an unloaded area, so you must predict their path.
  - Distant Sight :: Characters have a field of vision (radius around them), and their patience is degraded over time if any unloaded areas are within that field, based on how many there are, and how close they are to the character.
- Game essence: The computer has finite memory, so only a /small/ number of areas can be loaded at once (will require tweaking).  It could also take time for an area to load or unload.
- Interface: Drag and drop, or simple click to toggle area loading?
  - Two input styles: Toggle and L/R.  The first is suited to touchscreens, the latter to a mouse.
    - Toggle :: Button that switches left click/tap between loading and unloading.  UI button and/or keyboard key.  Also swaps right click, keeping them opposites.
    - L/R :: Left click always loads, right click always unloads.
- If the interaction mechanics are deep enough, the world itself could be a simple grid.
  The implication of there being a large number of small areas points in favour of a paintbrush-style interface.
- Character Movement Patterns: Colour-coded, bearing in mind colour blindness!
  - Focused :: Moves between locations quickly, in straight lines.
  - Wanderer :: Meandering, unpredictable but slow path.
    Could probably be implemented by changing the parameters for a focused character.
  - Teleporting :: Immediately hops from one area to another, anywhere in the world.
- Automatic Difficulty: Characters enter and exit the world over the course of the game, and leave sooner if the player can't keep up with their loading demands.
- Clear Win Condition: Every character reaches their objective and stops playing?
**** Character Artificial Intelligence
- States :: Waiting for Level to Load, Meandering Aimlessly, Moving Towards An Objective.
- Parameters ::
  - Target Speed :: Speed with which character moves towards destination.
  - Wander Speed :: Speed with which character moves aimlessly.
    Should be separate from target speed, and almost certainly lower, to compensate for the lower predictability.
  - Focus :: Affects how likely character is to be in meandering/objective-seeking mode and switch between them.
  - Jitter :: How much extra movement off-course a character does while moving towards an objective.
    Probably unnecessary.
**** Graphics
- Programmer Art :: Block colours for area tiles, circles for characters.
- Real Art :: Simple terrain (plains, mountain, sea, etc.), arrow showing direction of characters.
- Specifications ::
  - Tiles :: 32×32
  - Characters :: 8×8
**** Title & Presentation
- "Faking the Space" (with reference to [[https://youtu.be/Q85l1Fenc5w][the Jacob Geller video]]).
**** Implementation
- Probably simple enough to do with C++ & OpenGL, but why?  I'm short on time anyway!
  - First things first: See if I can build an OpenGL project on Windows. ✓
  - Ok, fine, but use raylib at least.
** Roles Outside Video Games
*** Papers Please
